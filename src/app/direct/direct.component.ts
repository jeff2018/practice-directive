import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-direct',
  templateUrl: './direct.component.html',
  styleUrls: ['./direct.component.css']
})
export class DirectComponent implements OnInit {

  today = [];
  directCreated = false;
  directs = [];
  Count = 0;

  constructor() { }

  ngOnInit() {
  }

  onCreateDirect() {
    this.directCreated = !this.directCreated;
    this.directs.push(this.Count);
    this.today[0] = Date.now();
    this.Count += 1;
  }
}
